﻿using System;
using CourseJournalMS.JsonReader.IO_Helpers;

namespace CourseJournalMS.JsonReader
{
    class ProgramLoop
    {
        private bool _exit = false;

        public void Execute()
        {
            string command;

            while (!_exit)
            {
                ConsoleWriteHelper.ShowReaderMenu();
                command = Console.ReadLine();
                switch (command)
                {
                    case "1":
                        ConsoleWriteHelper.ShowAvalibleJsonFiles();
                        break;
                    case "2":
                        Show();
                        break;
                    case "Q":
                        _exit = true;
                        break;
                    default:
                        Console.WriteLine("Command not found. Try again...");
                        break;
                }
            }
        }

        private void Show()
        {
            var reportService = new ReaderReportService();

            var file = ConsoleReadHelper.GetJsonNameToShow();
            var reportToShow = reportService.ReportPrepare(file);
            var output = reportService.GenerateReportString(reportToShow);
            Console.WriteLine(output);
        }
    }
}
