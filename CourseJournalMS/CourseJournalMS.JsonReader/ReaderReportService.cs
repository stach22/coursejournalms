﻿using System;
using System.Collections.Generic;
using System.Linq;
using CourseJournalMS.BusinessLayer.Dtos;
using CourseJournalMS.BusinessLayer.Mappers;
using CourseJournalMS.DataLayer.Repositories;

namespace CourseJournalMS.JsonReader
{
    public class ReaderReportService
    {
        public ReportDto ReportPrepare(string file)
        {
            var jSon = new FilesRepo<ReportDto>(new JsonMapper());

            var reportDto = jSon.Read(AppDomain.CurrentDomain.BaseDirectory + file);

            return reportDto;
        }

        public string GenerateReportString(ReportDto reportDto)
        {
            var message = "";
            var studentsNo = reportDto.StudentsPresence.Count();
            var homeworkAvg = "Average homework result: " + HomeworksAverage(reportDto).ToString("0.00") + "%";
            var presencesAvg = "Average presence: " + PresencesAverage(reportDto).ToString("0.00") + "%";
            var homeworkPassedStudents = "Students which have passed homeworks threshold: " + StudentHomeworksPassedCount(reportDto);
            var presencePassedStudents = "Students which have passed presences threshold: " + StudentPresencesPassedCount(reportDto);

            message += "Course Name: " + reportDto.CourseName + "\n" + "Course Teacher: " + reportDto.TeacherName +
                "\n" + homeworkAvg + "\n" + presencesAvg + "\n" + homeworkPassedStudents + "/" + studentsNo + 
                "\n" + presencePassedStudents + "/" + studentsNo + "\n";

            return message;
        }

        private double HomeworksAverage(ReportDto reportDto)
        {
            var studentsHomeworks = reportDto.StudentsHomeworks;

            List<double> studentsResults = new List<double>();

            foreach (var homework in studentsHomeworks)
            {
                var resultPercent = homework.HomeworkPercent;
                studentsResults.Add(resultPercent);
            }

            var average = studentsResults.Average();
            return average;
        }

        private double PresencesAverage(ReportDto reportDto)
        {
            var studentPresences = reportDto.StudentsPresence;
            List<double> presencesList = new List<double>();

            foreach (var presence in studentPresences)
            {
                var resultPercent = presence.PresencesPercent;
                presencesList.Add(resultPercent);
            }
            var average = presencesList.Average();
            return average;
        }

        private int StudentHomeworksPassedCount(ReportDto reportDto)
        {
            var studentHomeworks = reportDto.StudentsHomeworks;
            List<string> studentsPassed = new List<string>();

            foreach (var student in studentHomeworks)
            {
                if (student.Passed == "Passed")
                {
                    studentsPassed.Add(student.Passed);
                }
            }
            var count = studentsPassed.Count;
            return count;
        }

        private int StudentPresencesPassedCount(ReportDto reportDto)
        {
            var studentPresences = reportDto.StudentsPresence;
            List<string> studentsPassed = new List<string>();

            foreach (var student in studentPresences)
            {
                if (student.Passed == "Passed")
                {
                    studentsPassed.Add(student.Passed);
                }
            }
            var count = studentsPassed.Count;
            return count;
        }
    }
}
