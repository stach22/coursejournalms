﻿using System;
using System.IO;

namespace CourseJournalMS.JsonReader.IO_Helpers
{
    public class ConsoleWriteHelper
    {
        public static void ShowAvalibleJsonFiles()
        {
            var baseDir = AppDomain.CurrentDomain.BaseDirectory;
            string[] filePaths = Directory.GetFiles(baseDir, "*.json");

            if (filePaths.Length == 0)
            {
                Console.WriteLine("There's no JSON files to read");
            }
            else
            {
                Console.WriteLine("Avalible json files in: " + baseDir + "\n");

                foreach (var file in filePaths)
                {
                    var onlyFileName = Path.GetFileName(file);

                    Console.WriteLine(onlyFileName);
                }
            }
            Console.WriteLine("\n");
        }

        public static void ShowReaderMenu()
        {
            Console.WriteLine("<1> Show avalible files\n<2> Read report from file\n<Q> Exit");
        }
    }
}
