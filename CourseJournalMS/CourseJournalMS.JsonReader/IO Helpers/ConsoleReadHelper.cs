﻿using System;
using System.IO;

namespace CourseJournalMS.JsonReader.IO_Helpers
{
    public class ConsoleReadHelper
    {
        public static string GetJsonNameToShow()
        {
            Console.WriteLine("Enter file name...");

            string userInput;
            bool success;

            do
            {
                userInput = Console.ReadLine();

                var filePath = AppDomain.CurrentDomain.BaseDirectory + userInput;
                var exist = File.Exists(filePath);

                success = exist;

                if (!success)
                {
                    Console.WriteLine("Wrong File Name...\nProvide name with extension, for example 'myCourseReport.json'");
                }
            } while (!success);

            return userInput;
        }
    }
}
