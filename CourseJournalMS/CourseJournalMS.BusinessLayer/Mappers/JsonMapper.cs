﻿using CourseJournalMS.BusinessLayer.Dtos;
using CourseJournalMS.DataLayer.Interfaces;
using Newtonsoft.Json;

namespace CourseJournalMS.BusinessLayer.Mappers
{
    public class JsonMapper : IContentMapper<ReportDto>
    {
        public string FromContent(ReportDto report)
        {
            return JsonConvert.SerializeObject(report, Formatting.Indented);
        }

        public ReportDto ToContent(string report)
        {
            return JsonConvert.DeserializeObject<ReportDto>(report);
        }
    }
}