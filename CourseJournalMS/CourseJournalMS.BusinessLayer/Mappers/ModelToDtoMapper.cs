﻿using CourseJournalMS.BusinessLayer.Dtos;
using CourseJournalMS.DataLayer.Models;

namespace CourseJournalMS.BusinessLayer.Mappers
{
    public class ModelToDtoMapper
    {
        public static StudentDto ModelToStudentDto(Student student)
        {
            var studentDto = new StudentDto();
            studentDto.Id = student.Id;
            studentDto.Pesel = student.Pesel;
            studentDto.Name = student.Name;
            studentDto.Surname = student.Surname;
            studentDto.Sex = student.Sex;
            studentDto.BirthDate = student.BirthDate;

            return studentDto;
        }

        public static CourseDto CourseModelToDtoMapper(Course course)
        {
            var courseDto = new CourseDto();

            courseDto.Id = course.Id;
            courseDto.CourseName = course.CourseName;
            courseDto.TeacherName = course.TeacherName;
            courseDto.StartTime = course.StartTime;
            courseDto.HomeworkRequired = course.HomeworkRequired;
            courseDto.PresenceRequired = course.PresenceRequired;

            return courseDto;
        }
    }
}