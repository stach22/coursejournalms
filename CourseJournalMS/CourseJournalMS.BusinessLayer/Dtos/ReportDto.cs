﻿using System;
using System.Collections.Generic;

namespace CourseJournalMS.BusinessLayer.Dtos
{
    public class ReportDto
    {
        public string CourseName;
        public string TeacherName;
        public DateTime StartTime;
        public int PresenceRequired;
        public int HomeworkRequired;
        public IEnumerable<StudentPresenceReportDto> StudentsPresence;
        public IEnumerable<StudentHomeworksReportDto> StudentsHomeworks;
    }
}
