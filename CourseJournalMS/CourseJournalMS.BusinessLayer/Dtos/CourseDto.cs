﻿using System;

namespace CourseJournalMS.BusinessLayer.Dtos
{
    public class CourseDto
    {
        public int Id;
        public string CourseName;
        public int HomeworkRequired;
        public int NumberOfStudents;
        public int PresenceRequired;
        public DateTime StartTime;
        public string TeacherName;

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var courseDto = obj as CourseDto;

            if (courseDto == null)
            {
                return false;
            }

            bool areEqual = true;
            areEqual &= Id == courseDto.Id;
            areEqual &= CourseName == courseDto.CourseName;
            areEqual &= TeacherName == courseDto.TeacherName;
            areEqual &= StartTime == courseDto.StartTime;
            areEqual &= HomeworkRequired == courseDto.HomeworkRequired;
            areEqual &= PresenceRequired == courseDto.PresenceRequired;

            return areEqual;
        }

    }
}