﻿namespace CourseJournalMS.BusinessLayer.Dtos
{
    public class HomeworkDto
    {
        public string HomeworkName;
        public int MaxPoints;
    }
}