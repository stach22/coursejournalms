﻿namespace CourseJournalMS.BusinessLayer.Dtos
{
    public class StudentHomeworksReportDto
    {
        public int StudentId;
        public string StudentName;
        public string StudentSurname;
        public int HomeworkPointsSum;
        public int HomeworkMaxPointsSum;
        public double HomeworkPercent;
        public string Passed;

    }
}
