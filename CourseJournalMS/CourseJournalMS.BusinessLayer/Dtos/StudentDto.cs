﻿using System;

namespace CourseJournalMS.BusinessLayer.Dtos
{
    public class StudentDto
    {
        public int Id;
        public DateTime BirthDate;
        public string Name;
        public long Pesel;
        public string Sex;
        public string Surname;

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var studentDto = obj as StudentDto;

            if (studentDto == null)
            {
                return false;
            }

            bool areEqual = true;
            areEqual &= Id == studentDto.Id;
            areEqual &= Pesel == studentDto.Pesel;
            areEqual &= Name == studentDto.Name;
            areEqual &= Surname == studentDto.Surname;
            areEqual &= Sex == studentDto.Sex;
            areEqual &= BirthDate == studentDto.BirthDate;

            return areEqual;
        }

    }
}