﻿using System;

namespace CourseJournalMS.BusinessLayer.Dtos
{
    public class CourseDayDto
    {
        public DateTime CourseDayDate;
    }
}