﻿namespace CourseJournalMS.BusinessLayer.Dtos
{
    public class StudentPresenceReportDto
    {
        public int StudentId;
        public string StudentName;
        public string StudentSurname;
        public int Presences;
        public int CourseDayCount;
        public double PresencesPercent;
        public string Passed;
    }
}
