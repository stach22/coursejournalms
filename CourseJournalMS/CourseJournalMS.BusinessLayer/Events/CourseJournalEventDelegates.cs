﻿using System;
using CourseJournalMS.BusinessLayer.Dtos;

namespace CourseJournalMS.BusinessLayer.Events
{
    public class CourseJournalEventDelegates : EventArgs
    {
        public delegate void GenerateReportEventHandler(object sender, ReportDto report);
    }
}