﻿using System;
using System.Collections.Generic;
using System.Linq;
using CourseJournalMS.BusinessLayer.Dtos;
using CourseJournalMS.BusinessLayer.Interfaces;
using CourseJournalMS.BusinessLayer.Mappers;
using CourseJournalMS.DataLayer.Interfaces;
using CourseJournalMS.DataLayer.Models;
using CourseJournalMS.DataLayer.Repositories;

namespace CourseJournalMS.BusinessLayer.Services
{
    public class ReportService : IReportService
    {
        private readonly ICourseRepoService _courseRepoService;
        private readonly IHomeworkRepoService _homeworkRepoService;
        private readonly IResultRepoService _resultRepoService;
        private readonly IPresencesRepoService _presencesRepoService;

        public ReportService(ICourseRepoService courseRepoService, IHomeworkRepoService homeworkRepoService,
            IResultRepoService resultRepoService, IPresencesRepoService presencesRepoService)
        {
            _courseRepoService = courseRepoService;
            _homeworkRepoService = homeworkRepoService;
            _presencesRepoService = presencesRepoService;
            _resultRepoService = resultRepoService;
        }

        private IEnumerable<StudentPresenceReportDto> GenereteStudentPresenceReportDto(Course course)
        {
            var studentPresenceReportDtos = new HashSet<StudentPresenceReportDto>();

            foreach (var student in course.Students)
            {
                var dto = new StudentPresenceReportDto();
                var presences = _presencesRepoService.GetPresenceForStudentInCourse(student, course);

                var courseDaysCount = course.CourseDays.Count;
                var presencesPercent = Percentage(presences, courseDaysCount);
                var passed = Passed(presencesPercent, course.PresenceRequired);

                dto.StudentId = student.Id;
                dto.StudentName = student.Name;
                dto.StudentSurname = student.Surname;
                dto.CourseDayCount = courseDaysCount;
                dto.Presences = presences;
                dto.PresencesPercent = presencesPercent;
                dto.Passed = passed;

                studentPresenceReportDtos.Add(dto);
            }
            return studentPresenceReportDtos;
        }

        public string GenerateReportString(ReportDto reportDto)
        {
            var report =
                "Course name: " + reportDto.CourseName + "\n" +
                "Course Teacher: " + reportDto.TeacherName + "\n" +
                "Course start date: " + reportDto.StartTime + "\n" +
                "Course presence required: " + reportDto.PresenceRequired + "%" + "\n" +
                "Course homework required: " + reportDto.HomeworkRequired + "%" + "\n" +
                "\nEach student's presence: " + "\n" +
                GenereteStudentPresenceReportString(reportDto) +
                "\nEach student's homeworks done: " + "\n" +
                GenereteStudentHomeworksReportString(reportDto);

            return report;
        }

        private string GenereteStudentHomeworksReportString(ReportDto reportDto)
        {
            var message = "";

            foreach (var student in reportDto.StudentsHomeworks)
            {
                message += student.StudentId + " " + student.StudentName + " " + student.StudentSurname + " "
                           + student.HomeworkPointsSum + "/" + student.HomeworkMaxPointsSum + " (" + student.HomeworkPercent.ToString("0.00") +
                           "%) - " + student.Passed + "\n";
            }
            return message;
        }

        private string GenereteStudentPresenceReportString(ReportDto reportDto)
        {
            var message = "";

            foreach (var student in reportDto.StudentsPresence)
            {
                
                message += student.StudentId + " " + student.StudentName + " " + student.StudentSurname + " "
                           + student.Presences + "/" + student.CourseDayCount + " (" + student.PresencesPercent.ToString("0.00") + "%) - " +
                           student.Passed + "\n";
            }
            return message;
        }

        public ReportDto PrepareReportDto(string courseName)
        {
            var course = _courseRepoService.GetAllCourseData().SingleOrDefault(c => c.CourseName == courseName);

            var reportDto = new ReportDto();
            reportDto.CourseName = course.CourseName;
            reportDto.TeacherName = course.TeacherName;
            reportDto.StartTime = course.StartTime;
            reportDto.PresenceRequired = course.PresenceRequired;
            reportDto.HomeworkRequired = course.HomeworkRequired;

            reportDto.StudentsPresence = GenereteStudentPresenceReportDto(course);

            reportDto.StudentsHomeworks = GenereteStudentHomeworksReportDto(course);

            return reportDto;
        }

        private IEnumerable<StudentHomeworksReportDto> GenereteStudentHomeworksReportDto(Course course)
        {
            var studentHomeworksReportDtos = new HashSet<StudentHomeworksReportDto>();

            foreach (var student in course.Students)
            {
                var dto = new StudentHomeworksReportDto();

                var homeworkPointsSum = _resultRepoService.GetPointsSumForStudentInCourse(student, course);
                var homeworkMaxPointsSum = _homeworkRepoService.GetMaxPointsSumInCourse(course);

                var homeworkPercent = Percentage(homeworkPointsSum, homeworkMaxPointsSum);
                var passed = Passed(homeworkPercent, course.HomeworkRequired);

                dto.StudentId = student.Id;
                dto.StudentName = student.Name;
                dto.StudentSurname = student.Surname;
                dto.HomeworkMaxPointsSum = homeworkMaxPointsSum;
                dto.HomeworkPointsSum = homeworkPointsSum;
                dto.HomeworkPercent = homeworkPercent;
                dto.Passed = passed;

                studentHomeworksReportDtos.Add(dto);
            }
            return studentHomeworksReportDtos;
        }

        public static string Passed(double percent, double threshold)
        {
            if (percent >= threshold)
            {
                return "Passed";
            }
            else
            {
                return "Failed";
            }
        }

        public static double Percentage(double a, double b)
        {
            if (b != 0)
            {
                return a / b * 100;
            }
            else
            {
                return 0;
            }
        }

        public bool ExportReportToFile(ReportDto report, string fileName)
        {
            var jSonSave = new FilesRepo<ReportDto>(new JsonMapper());

            jSonSave.Save(AppDomain.CurrentDomain.BaseDirectory + @"\" + fileName + ".json", report);

            return true;
        }
    }
}