﻿using System.Collections.Generic;
using System.Linq;
using CourseJournalMS.BusinessLayer.Dtos;
using CourseJournalMS.BusinessLayer.Interfaces;
using CourseJournalMS.BusinessLayer.Mappers;
using CourseJournalMS.DataLayer.Interfaces;
using CourseJournalMS.DataLayer.Models;
using CourseJournalMS.DataLayer.Repositories;

namespace CourseJournalMS.BusinessLayer.Services
{
    public class CourseService : ICourseService
    {
        private readonly IGenericRepo<Course> _courseRepo;
        private readonly UnitOfWorkCourseJournal _unitOfWork;
        private readonly ICourseRepoService _courseRepoService;
        private readonly IStudentRepoService _studentRepoService;

        public CourseService(UnitOfWorkCourseJournal unitOfWork, 
            ICourseRepoService courseRepoService, IStudentRepoService studentRepoService)
        {
            var courseRepo = unitOfWork.GetRepo<Course>();
            _unitOfWork = unitOfWork;
            _courseRepo = courseRepo;
            _courseRepoService = courseRepoService;
            _studentRepoService = studentRepoService;
        }

        public List<StudentDto> GetStudentDtos(string courseName)
        {
            var course = _courseRepoService.GetAllCourseData().SingleOrDefault(c => c.CourseName == courseName);

            var studentDtos = new List<StudentDto>();

            foreach (var student in course.Students)
            {
                studentDtos.Add(ModelToDtoMapper.ModelToStudentDto(student));
            }

            return studentDtos;
        }

        public void CreateCourseWithStudents(CourseDto courseDto, List<long> studentPesels)
        {
            var course = DtoToModelMapper.CourseDtoToModelMapper(courseDto);

            var students = new List<Student>();

            foreach (var pesel in studentPesels)
            {
                var student = _studentRepoService.GetStudentDataIncludingCourses().SingleOrDefault(s => s.Pesel == pesel);
                students.Add(student);
            }

            course.Students = students;

            _courseRepo.Add(course);
            _unitOfWork.SaveChanges();
        }

        public bool CheckIfCourseExistByName(string courseName)
        {
            var course = _courseRepo.GetAll().SingleOrDefault(c => c.CourseName == courseName);

            if (course == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public CourseDto GetCourseDataByName(string courseName)
        {
            var course = _courseRepo.GetAll().SingleOrDefault(c => c.CourseName == courseName);

            var courseDto = ModelToDtoMapper.CourseModelToDtoMapper(course);

            return courseDto;
        }

        public void UpdateCourse(CourseDto courseDto)
        {
            var course = DtoToModelMapper.CourseDtoToModelMapper(courseDto);

            _courseRepo.Update(course);
            _unitOfWork.SaveChanges();
        }
    }
}