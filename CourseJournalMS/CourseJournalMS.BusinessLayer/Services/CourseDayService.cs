﻿using System.Collections.Generic;
using System.Linq;
using CourseJournalMS.BusinessLayer.Dtos;
using CourseJournalMS.BusinessLayer.Interfaces;
using CourseJournalMS.BusinessLayer.Mappers;
using CourseJournalMS.DataLayer.Interfaces;
using CourseJournalMS.DataLayer.Models;
using CourseJournalMS.DataLayer.Repositories;

namespace CourseJournalMS.BusinessLayer.Services
{
    public class CourseDayService : ICourseDayService
    {
        private readonly IGenericRepo<CourseDay> _courseDayRepo;
        private readonly IGenericRepo<Presence> _presencesRepo;
        private readonly UnitOfWorkCourseJournal _unitOfWork;
        private readonly ICourseRepoService _courseRepoService;

        public CourseDayService(UnitOfWorkCourseJournal unitOfWork, ICourseRepoService courseRepoService)
        {
            var courseDayRepo = unitOfWork.GetRepo<CourseDay>();
            var presencesRepo = unitOfWork.GetRepo<Presence>();
            _unitOfWork = unitOfWork;
            _courseDayRepo = courseDayRepo;
            _presencesRepo = presencesRepo;
            _courseRepoService = courseRepoService;
        }

        public void CreateCourseDay(CourseDayDto courseDayDto, string courseName, List<PresenceDto> presenceDtos)
        {
            var courseDay = DtoToModelMapper.CourseDayDtoToModelMapper(courseDayDto);
            var course = _courseRepoService.GetAllCourseData().SingleOrDefault(c => c.CourseName == courseName);

            courseDay.Course = course;

            var presences = new List<Presence>();
            foreach (var student in course.Students)
            {
                var presenceDto = presenceDtos.Find(p => p.Pesel == student.Pesel);
                var presence = DtoToModelMapper.PresenceDtoToModelMapper(presenceDto);

                presence.Student = student;
                presence.CourseDay = courseDay;

                presences.Add(presence);
            }

             _courseDayRepo.Add(courseDay);

            foreach (var presence in presences)
            {
                _presencesRepo.Add(presence);
            }
            _unitOfWork.SaveChanges();
        }
    }
}