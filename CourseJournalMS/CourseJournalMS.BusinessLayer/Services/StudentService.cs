﻿using System.Linq;
using CourseJournalMS.BusinessLayer.Dtos;
using CourseJournalMS.BusinessLayer.Interfaces;
using CourseJournalMS.BusinessLayer.Mappers;
using CourseJournalMS.DataLayer.Interfaces;
using CourseJournalMS.DataLayer.Models;
using CourseJournalMS.DataLayer.Repositories;

namespace CourseJournalMS.BusinessLayer.Services
{
    public class StudentService : IStudentService
    {
        private readonly IGenericRepo<Student> _studentRepo;
        private readonly IGenericRepo<Course> _courseRepo;
        private readonly UnitOfWorkCourseJournal _unitOfWork;
        private readonly IStudentRepoService _studentRepoService;
        private readonly ICourseRepoService _courseRepoService;

        public StudentService(UnitOfWorkCourseJournal unitOfWork, IStudentRepoService studentRepoService,
            ICourseRepoService courseRepoService)
        {
            var studentRepo = unitOfWork.GetRepo<Student>();
            var courseRepo = unitOfWork.GetRepo<Course>();
            _unitOfWork = unitOfWork;
            _studentRepo = studentRepo;
            _courseRepo = courseRepo;
            _studentRepoService = studentRepoService;
            _courseRepoService = courseRepoService;
        }

        public void CreateStudent(StudentDto studentDto)
        {
            var student = DtoToModelMapper.StudentDtoToModelMapper(studentDto);

            _studentRepo.Add(student);
            _unitOfWork.SaveChanges();
        }

        public bool CheckIfStudentExistByPesel(long pesel)
        {
            var student = _studentRepo.GetAll().SingleOrDefault(s => s.Pesel == pesel);

            if (student == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public StudentDto GetStudentDataByPesel(long pesel)
        {
            var student = _studentRepoService.GetStudentDataIncludingCourses().Single(s => s.Pesel == pesel);

            var studentDto = ModelToDtoMapper.ModelToStudentDto(student);

            return studentDto;
        }

        public void UpdateStudent(StudentDto studentDto)
        {
            var student = DtoToModelMapper.StudentDtoToModelMapper(studentDto);

            _studentRepo.Update(student);
            _unitOfWork.SaveChanges();
        }

        public void RemoveStudentFromCourse(StudentDto studentDto, string courseName)
        {
            var student = _studentRepoService.GetStudentDataIncludingCourses().Single(s => s.Pesel == studentDto.Pesel);

            var course = _courseRepoService.GetAllCourseData().Single(c => c.CourseName == courseName);

            _courseRepoService.RemoveStudentFromCourse(course, student);
            _unitOfWork.SaveChanges();
        }
    }
}