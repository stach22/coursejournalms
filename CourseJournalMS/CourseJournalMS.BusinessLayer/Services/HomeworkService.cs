﻿using System.Collections.Generic;
using System.Linq;
using CourseJournalMS.BusinessLayer.Dtos;
using CourseJournalMS.BusinessLayer.Interfaces;
using CourseJournalMS.BusinessLayer.Mappers;
using CourseJournalMS.DataLayer.Interfaces;
using CourseJournalMS.DataLayer.Models;
using CourseJournalMS.DataLayer.Repositories;

namespace CourseJournalMS.BusinessLayer.Services
{
    public class HomeworkService : IHomeworkService
    {
        private readonly IGenericRepo<Homework> _homeworkRepo;
        private readonly IGenericRepo<Result> _resultRepo;

        private readonly UnitOfWorkCourseJournal _unitOfWork;
        private readonly ICourseRepoService _courseRepoService;

        public HomeworkService(UnitOfWorkCourseJournal unitOfWork, ICourseRepoService courseRepoService)
        {
            var homeworkRepo = unitOfWork.GetRepo<Homework>();
            var resultRepo = unitOfWork.GetRepo<Result>();
            _unitOfWork = unitOfWork;
            _homeworkRepo = homeworkRepo;
            _resultRepo = resultRepo;
            _courseRepoService = courseRepoService;
        }

        public void CreateHomework(HomeworkDto homeworkDto, string courseName, List<ResultDto> resultDtos)
        {
            var homework = DtoToModelMapper.HomeworkDtoToModelMapper(homeworkDto);
            var course = _courseRepoService.GetAllCourseData().SingleOrDefault(c => c.CourseName == courseName);

            homework.Course = course;

            var results = new List<Result>();

            foreach (var student in course.Students)
            {
                var result = new Result();
                result.Value = resultDtos.Find(r => r.Pesel == student.Pesel).Value;
                result.Student = student;
                result.Homework = homework;

                results.Add(result);
            }

            _homeworkRepo.Add(homework);

            foreach (var result in results)
            {
                _resultRepo.Add(result);
            }

            _unitOfWork.SaveChanges();
        }
    }
}