﻿using CourseJournalMS.BusinessLayer.Interfaces;
using CourseJournalMS.BusinessLayer.Services;
using Ninject.Modules;

namespace CourseJournalMS.BusinessLayer.Modules
{
    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ICourseDayService>().To<CourseDayService>();
            Bind<ICourseService>().To<CourseService>();
            Bind<IHomeworkService>().To<HomeworkService>();
            Bind<IStudentService>().To<StudentService>();
            Bind<IReportService>().To<ReportService>();
        }
    }
}
