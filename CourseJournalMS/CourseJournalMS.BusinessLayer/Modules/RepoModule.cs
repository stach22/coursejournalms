﻿using CourseJournalMS.DataLayer.Interfaces;
using CourseJournalMS.DataLayer.Repositories;
using Ninject.Modules;

namespace CourseJournalMS.BusinessLayer.Modules
{
    public class RepoModule : NinjectModule
    {
        public override void Load()
        {
            Bind<UnitOfWorkCourseJournal>().ToConstant(UnitOfWorkCourseJournal.GetInstance());

            Bind(typeof(IGenericRepo<>)).To(typeof(GenericRepo<>));
        }
    }
}
