﻿using CourseJournalMS.DataLayer.Interfaces;
using CourseJournalMS.DataLayer.Services;
using Ninject.Modules;

namespace CourseJournalMS.BusinessLayer.Modules
{
    public class RepoServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ICourseRepoService>().To<CourseRepoService>();
            Bind<IHomeworkRepoService>().To<HomeworkRepoService>();
            Bind<IPresencesRepoService>().To<PresencesRepoService>();
            Bind<IResultRepoService>().To<ResultRepoService>();
            Bind<IStudentRepoService>().To<StudentRepoService>();
        }
    }
}
