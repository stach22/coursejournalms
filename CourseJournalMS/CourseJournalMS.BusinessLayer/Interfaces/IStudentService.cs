using CourseJournalMS.BusinessLayer.Dtos;

namespace CourseJournalMS.BusinessLayer.Interfaces
{
    public interface IStudentService
    {
        void CreateStudent(StudentDto studentDto);
        bool CheckIfStudentExistByPesel(long pesel);
        StudentDto GetStudentDataByPesel(long pesel);
        void UpdateStudent(StudentDto studentDto);
        void RemoveStudentFromCourse(StudentDto studentDto, string courseName);
    }
}