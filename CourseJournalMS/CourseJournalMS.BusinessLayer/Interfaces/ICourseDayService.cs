using System.Collections.Generic;
using CourseJournalMS.BusinessLayer.Dtos;

namespace CourseJournalMS.BusinessLayer.Interfaces
{
    public interface ICourseDayService
    {
        void CreateCourseDay(CourseDayDto courseDayDto, string courseName, List<PresenceDto> presenceDtos);
    }
}