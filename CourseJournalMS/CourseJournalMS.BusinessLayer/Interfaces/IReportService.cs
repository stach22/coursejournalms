using CourseJournalMS.BusinessLayer.Dtos;

namespace CourseJournalMS.BusinessLayer.Interfaces
{
    public interface IReportService
    {
        string GenerateReportString(ReportDto reportDto);
        bool ExportReportToFile(ReportDto report, string fileName);
        ReportDto PrepareReportDto(string courseName);
    }
}