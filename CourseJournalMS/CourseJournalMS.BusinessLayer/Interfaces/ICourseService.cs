using System.Collections.Generic;
using CourseJournalMS.BusinessLayer.Dtos;

namespace CourseJournalMS.BusinessLayer.Interfaces
{
    public interface ICourseService
    {
        List<StudentDto> GetStudentDtos(string courseName);
        void CreateCourseWithStudents(CourseDto courseDto, List<long> studentPesels);
        bool CheckIfCourseExistByName(string courseName);
        CourseDto GetCourseDataByName(string courseName);
        void UpdateCourse(CourseDto courseDto);
    }
}