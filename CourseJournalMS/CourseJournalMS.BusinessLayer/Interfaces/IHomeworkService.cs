using System.Collections.Generic;
using CourseJournalMS.BusinessLayer.Dtos;

namespace CourseJournalMS.BusinessLayer.Interfaces
{
    public interface IHomeworkService
    {
        void CreateHomework(HomeworkDto homeworkDto, string courseName, List<ResultDto> resultDtos);
    }
}