﻿using System;
using CourseJournalMS.BusinessLayer.Dtos;

namespace CourseJournalMS.CliLayer.IoHelpers
{
    public class ConsoleWriteHelper
    {
        public static void OperationSuccessMessage(bool success)
        {
            if (success)
            {
                Console.WriteLine("Operation succeeded");
                Console.WriteLine("\nPress any key...");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Operation failed");
            }
        }

        public static string StudentCurrentData(StudentDto studentDto)
        {
            return "\nStudent's current info:\nName: " + studentDto.Name + "\nSurname: " +
                              studentDto.Surname + "\nSex: " + studentDto.Sex + "\nBirth Date: " + studentDto.BirthDate;
        }

        public static string CourseCurrentData(CourseDto courseDto)
        {
            return "\nCourse current info:\nCourse name: " + courseDto.CourseName + "\nTeacher name: " +
                   courseDto.TeacherName + "\nMin Homeworks % required: " + courseDto.HomeworkRequired 
                   + "\nMin Presence % required: " + courseDto.PresenceRequired;
        }

        public static void FileSaveSuccessMessage(bool success)
        {
            if (success)
            {
                Console.Clear();
                Console.WriteLine("File successfully saved in:\n\n" + AppDomain.CurrentDomain.BaseDirectory);
                Console.WriteLine("\nPress any key...");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Operation failed");
            }
        }
    }
}
