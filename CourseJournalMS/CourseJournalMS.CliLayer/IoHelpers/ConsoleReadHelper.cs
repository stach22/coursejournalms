﻿using System;

namespace CourseJournalMS.CliLayer.IoHelpers
{
    public class ConsoleReadHelper
    {
        public static string GetNonEmptyInput()
        {
            string input = Console.ReadLine();

            while (string.IsNullOrEmpty(input))
            {
                Console.WriteLine("Type input value...");
                input = Console.ReadLine();
            }
            return input;
        }

        public static int GetInt()
        {
            int number;

            while (!int.TryParse(Console.ReadLine(), out number) || number < 0)
            {
                Console.WriteLine("Not a numeric value or input out of range (0-max) - try again...");
            }

            return number;
        }

        public static long GetPesel()
        {
            long peselOut;
            string peselIn = null;
            bool parseSuccessful;
            do
            {
                if (peselIn != null)
                {
                    Console.WriteLine("Wrong input value!");
                }
                Console.WriteLine("Enter PESEL: ");

                peselIn = Console.ReadLine();

                parseSuccessful = Int64.TryParse(peselIn, out peselOut);

                if (peselOut.ToString().Length != 11)
                {
                    parseSuccessful = false;
                }
            } while (parseSuccessful == false);

            return peselOut;
        }

        public static DateTime GetBirthDateTime()
        {
            DateTime dateTime;
            string date = null;
            do
            {
                if (date != null)
                {
                    Console.WriteLine("Wrong input value! Please type date.");
                }
                Console.WriteLine("Enter date of birth: ");
                date = Console.ReadLine();
            } while (!DateTime.TryParse(date, out dateTime));
            return dateTime;
        }

        public static string GetSex()
        {
            string sexIn = null;
            do
            {
                if (sexIn != null)
                {
                    Console.WriteLine("Wrong input value! Please type <M> or <F>. There's no more genders ;)");
                }
                Console.WriteLine("Enter student sex <M/F>");

                sexIn = Console.ReadLine();

            } while (sexIn != "M" && sexIn != "F");
            return sexIn;
        }

        public static DateTime GetStartDateTime()
        {
            DateTime dateTime;
            string date = null;
            do
            {
                if (date != null)
                {
                    Console.WriteLine("Wrong input value! Please type date.");
                }
                Console.WriteLine("Enter start date: ");
                date = Console.ReadLine();
            } while (!DateTime.TryParse(date, out dateTime));
            return dateTime;
        }

        public static int GetReqInt()
        {
            int number;

            while (!int.TryParse(Console.ReadLine(), out number) || number < 0 || number > 100)
            {
                Console.WriteLine("Not a numeric value or input out of range (0-100) - try again...");
            }

            return number;
        }

        public static DateTime GetCourseDayDateTime()
        {
            DateTime dateTime;
            string date = null;
            do
            {
                if (date != null)
                {
                    Console.WriteLine("Wrong input value! Please type date.");
                }
                Console.WriteLine("Enter date of course day: ");
                date = Console.ReadLine();
            } while (DateTime.TryParse(date, out dateTime) == false);
            return dateTime;
        }

        public static string GetChangeDecision()
        {
            string input;
            do
            {
                input = Console.ReadLine();
                if (input == "1" || input == "2" || input == "3" || input == "4" || input == "5")
                {
                    break;
                }
                Console.Write("Not a proper command type - try again...");
            } while (input != "1" || input != "2" || input != "3" || input != "4" || input != "5");
            return input;
        }

    }
}
