﻿using System;
using System.Text.RegularExpressions;

namespace CourseJournalMS.CliLayer.IoHelpers
{
    public class RegExpValidation
    {
        public static string GetCourseNameRegEx()
        {
            string userInput;
            bool success;

            do
            {
                userInput = Console.ReadLine();
                var match = Regex.Match(userInput, @"^C#_(.{1,})_([A-Z]{2})$");
                success = match.Success;
                if (!success)
                {
                    Console.WriteLine("Wrong Course Name.\n" +
                                      "Provide Name according to this pattern: 'C#_<any name>_<two uppercase letters>'\n" +
                                      "For example 'C#_anyName_MS'");
                }
            } while (!success);

            return userInput;
        }

        public static string GetFileNameToSaveRegEx()
        {
            Console.WriteLine("Enter file name...");

            string userInput;
            bool success;

            do
            {
                userInput = Console.ReadLine();
                var match = Regex.Match(userInput, @"^([\w]{1,})$");
                success = match.Success;
                if (!success)
                {
                    Console.WriteLine("Wrong File Name.\nEnter name without special characters," +
                                      " and without file extension.\nFor example: 'MyCourseReportFile'");
                }
            } while (!success);

            return userInput;
        }
    }
}
