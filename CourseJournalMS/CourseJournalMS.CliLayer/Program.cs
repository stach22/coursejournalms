﻿using CourseJournalMS.BusinessLayer.Modules;
using Ninject;

namespace CourseJournalMS.CliLayer
{
    class Program
    {
        private readonly IKernel _kernel = new StandardKernel(new ServiceModule(), new RepoModule(), new RepoServiceModule());

        static void Main()
        {
            new Program().Execute();
        }

        public void Execute()
        {
            var app = _kernel.Get<ProgramLoop>();
            app.StartWork();
        }
    }
}