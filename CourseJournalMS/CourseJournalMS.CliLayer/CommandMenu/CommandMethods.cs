﻿using System;
using System.Collections.Generic;

namespace CourseJournalMS.CliLayer.CommandMenu
{
    public class CommandMethods
    {
        private readonly Dictionary<string, MenuItem> _menuActions = new Dictionary<string, MenuItem>();
        
        public string GetMenuCommandFromUser()
        {
            string userInput = Console.ReadLine();

            while (string.IsNullOrEmpty(userInput) || !_menuActions.ContainsKey(userInput))
            {
                Console.WriteLine("Command not found. Try again...");
                userInput = Console.ReadLine();
            }

            return userInput;
        }

        public void AddCommand(string command, string name, Action action)
        {
            _menuActions.Add(command, new MenuItem(name, action));
        }

        public void RunCommand(string userInput)
        {
            _menuActions[userInput].CommandAction.Invoke();
        }

        public void ShowMenu()
        {
            Console.Clear();
            foreach (var command in _menuActions.Values)
            {
                Console.WriteLine(command.Name);
            }
        }
    }
}
