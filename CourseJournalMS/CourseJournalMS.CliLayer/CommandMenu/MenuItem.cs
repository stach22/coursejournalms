﻿using System;

namespace CourseJournalMS.CliLayer.CommandMenu
{
    public class MenuItem
    {
        public string Name { get; }

        public Action CommandAction { get;  }

        public MenuItem(string name, Action command)
        {
            Name = name;
            CommandAction = command;
        }
    }
}