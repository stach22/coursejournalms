﻿using System;
using System.Collections.Generic;
using CourseJournalMS.BusinessLayer.Dtos;
using CourseJournalMS.BusinessLayer.Events;
using CourseJournalMS.BusinessLayer.Interfaces;
using CourseJournalMS.CliLayer.CommandMenu;
using CourseJournalMS.CliLayer.IoHelpers;
using Ninject;

namespace CourseJournalMS.CliLayer
{
    public class ProgramLoop
    {
        private readonly ICourseDayService _courseDayService;
        private readonly ICourseService _courseService;
        private readonly IHomeworkService _homeworkService;
        private readonly IReportService _reportService;
        private readonly IStudentService _studentService;
        private bool _exit = false;

        public event CourseJournalEventDelegates.GenerateReportEventHandler ReportHandler;

        private readonly CommandMethods _mainMenuCommandMethods = new CommandMethods();
        private readonly CommandMethods _changeInfoMenuCommandMethods = new CommandMethods();

        [Inject]
        public ProgramLoop(ICourseDayService courseDayService, ICourseService courseService,
            IHomeworkService homeworkService, IReportService reportService, IStudentService studentService)
        {
            _courseDayService = courseDayService;
            _courseService = courseService;
            _homeworkService = homeworkService;
            _reportService = reportService;
            _studentService = studentService;
        }

        public void Work()
        {
            while (!_exit)
            {
                _mainMenuCommandMethods.ShowMenu();
                string userInput = _mainMenuCommandMethods.GetMenuCommandFromUser();

                _mainMenuCommandMethods.RunCommand(userInput);

                if (userInput == "6")
                {
                    string userInputInside = _changeInfoMenuCommandMethods.GetMenuCommandFromUser();
                    _changeInfoMenuCommandMethods.RunCommand(userInputInside);
                }
            }
        }

        public void InitializeCommands()
        {
            _mainMenuCommandMethods.AddCommand("1", "<1> Create Course", AddCourse);
            _mainMenuCommandMethods.AddCommand("2", "<2> Add Student", AddStudent);
            _mainMenuCommandMethods.AddCommand("3", "<3> Add Course Day", AddCourseDay);
            _mainMenuCommandMethods.AddCommand("4", "<4> Add Homework", AddHomework);
            _mainMenuCommandMethods.AddCommand("5", "<5> Show Report", GenerateReport);
            _mainMenuCommandMethods.AddCommand("6", "<6> Change Info", _changeInfoMenuCommandMethods.ShowMenu);
            _mainMenuCommandMethods.AddCommand("Q", "<Q> Exit", Exit);

            _changeInfoMenuCommandMethods.AddCommand("1", "<1> Change Student Info", ChangeStudentInfo);
            _changeInfoMenuCommandMethods.AddCommand("2", "<2> Change Course Info", ChangeCourseInfo);
            _changeInfoMenuCommandMethods.AddCommand("Q", "<Q> Back To Main Menu", Work);
        }

        public void StartWork()
        {
            InitializeCommands();
            ReportHandler += ReportJsonExportQuestion;
            Work();
        }

        private void Exit()
        {
            _exit = true;
        }

        public void ShowReport(ReportDto reportDto)
        {
            var report =_reportService.GenerateReportString(reportDto);
            Console.Clear();
            Console.WriteLine(report);
            if (reportDto != null)
            {
                Console.WriteLine("Report generated successfully. Press any key...");
                Console.ReadKey();
            }
        }

        public void GenerateReport()
        {
            var courseName = GetExistingCourseName();
            var reportDto = _reportService.PrepareReportDto(courseName);

            ShowReport(reportDto);

            OnReportGenerated(reportDto);
        }

        private void AddCourse()
        {
            var course = new CourseDto();

            course.CourseName = GetNewCourseName(course);
            Console.WriteLine("Enter teacher name: ");
            course.TeacherName = ConsoleReadHelper.GetNonEmptyInput();
            course.StartTime = ConsoleReadHelper.GetStartDateTime();
            Console.WriteLine("Set min value of homeworks completed (in percent): ");
            course.HomeworkRequired = ConsoleReadHelper.GetReqInt();
            Console.WriteLine("Set min value of presence required (in percent): ");
            course.PresenceRequired = ConsoleReadHelper.GetReqInt();
            Console.WriteLine("How many students you want to add?: ");
            course.NumberOfStudents = ConsoleReadHelper.GetInt();

            var studentPesels = new List<long>();

            for (int i = 1; i <= course.NumberOfStudents; i++)
            {
                bool studentAlreadyExists;
                do
                {
                    long studentPesel = ConsoleReadHelper.GetPesel();
                    studentAlreadyExists = _studentService.CheckIfStudentExistByPesel(studentPesel);
                    if (studentAlreadyExists)
                    {
                        if (studentPesels.Contains(studentPesel))
                        {
                            Console.WriteLine("There's already student in course with this PESEL. Try Again...");
                            studentAlreadyExists = false;
                        }
                        else
                        {
                            studentPesels.Add(studentPesel);
                        }
                    }
                    else
                    {
                        Console.WriteLine("There's no student in DataBase with this PESEL. Try Again...");
                    }

                } while (!studentAlreadyExists);
            }

            _courseService.CreateCourseWithStudents(course, studentPesels);
        }

        private void AddStudent()
        {
            var student = new StudentDto();
            bool studentAlreadyExists;
            do
            {
                student.Pesel = ConsoleReadHelper.GetPesel();
                studentAlreadyExists = _studentService.CheckIfStudentExistByPesel(student.Pesel);
                if (studentAlreadyExists)
                {
                    Console.WriteLine("Student already exists in DataBase!");
                }
            } while (studentAlreadyExists);

            Console.WriteLine("Enter Name: ");
            student.Name = ConsoleReadHelper.GetNonEmptyInput();
            Console.WriteLine("Enter Surname: ");
            student.Surname = ConsoleReadHelper.GetNonEmptyInput();
            student.Sex = ConsoleReadHelper.GetSex();
            student.BirthDate = ConsoleReadHelper.GetBirthDateTime();
            
            _studentService.CreateStudent(student);
        }

        private void AddCourseDay()
        {
            var courseDay = new CourseDayDto();

            var courseName = GetExistingCourseName();

            courseDay.CourseDayDate = ConsoleReadHelper.GetCourseDayDateTime();

            var studentDtos = _courseService.GetStudentDtos(courseName);

            var presenceDtos = new List<PresenceDto>();
            foreach (var studentDto in studentDtos)
            {
                Console.WriteLine(studentDto.Name + " " + studentDto.Surname);

                PresenceDto.PresenceEnum presenceEnum;
                string presenceTyped = null;
                do
                {
                    if (presenceTyped != null)
                    {
                        Console.WriteLine("Wrong input value! Please type <Present> or <Absent>.");
                    }
                    Console.WriteLine("mark presence (Present/Absent): ");
                    presenceTyped = Console.ReadLine();
                } while (Enum.TryParse(presenceTyped, out presenceEnum) == false);

                var presence = new PresenceDto();
                presence.Pesel = studentDto.Pesel;
                presence.Value = presenceEnum;
                presenceDtos.Add(presence);
            }

            _courseDayService.CreateCourseDay(courseDay, courseName, presenceDtos);
        }

        private void AddHomework()
        {
            var homework = new HomeworkDto();
            var courseName = GetExistingCourseName();

            Console.WriteLine("Enter homework name: ");
            homework.HomeworkName = ConsoleReadHelper.GetNonEmptyInput();
            Console.WriteLine("Enter max points of homework: ");
            homework.MaxPoints = ConsoleReadHelper.GetInt();

            var studentDtos = _courseService.GetStudentDtos(courseName);

            var results = new List<ResultDto>();
            foreach (var studentDto in studentDtos)
            {
                Console.WriteLine(studentDto.Name + " " + studentDto.Surname);

                int homeworkResult;
                string studentResult = null;
                bool parseSuccessful;
                do
                {
                    if (studentResult != null)
                    {
                        Console.WriteLine(
                            "Wrong input value! Please type a numeric value less than max points of homework.");
                    }
                    Console.WriteLine("Enter student's points: ");

                    studentResult = Console.ReadLine();

                    parseSuccessful = Int32.TryParse(studentResult, out homeworkResult);
                    if (homeworkResult > homework.MaxPoints || homeworkResult < 0)
                    {
                        parseSuccessful = false;
                    }
                } while (!parseSuccessful);

                ResultDto result = new ResultDto();
                result.Pesel = studentDto.Pesel;
                result.Value = homeworkResult;
                results.Add(result);
            }

            _homeworkService.CreateHomework(homework, courseName, results);
        }

        private void ChangeStudentInfo()
        {
            bool studentAlreadyExists;
            long pesel;
            var courseDto = new CourseDto();

            do
            {
                pesel = ConsoleReadHelper.GetPesel();

                studentAlreadyExists = _studentService.CheckIfStudentExistByPesel(pesel);
                if (!studentAlreadyExists)
                {
                    Console.WriteLine("There's no student in DataBase with this PESEL. Try Again...");
                }
            } while (!studentAlreadyExists);

            var studentDto = _studentService.GetStudentDataByPesel(pesel);

            Console.WriteLine(ConsoleWriteHelper.StudentCurrentData(studentDto) + "\n");

            do
            {
                Console.WriteLine("Choose action:\n<1> Change name\n<2> Change surname" +
                                  "\n<3> Change sex\n<4> Change date of birth\n<5> Sign out from course");

                var userChoose = ConsoleReadHelper.GetChangeDecision();

                if (userChoose == "1")
                {
                    Console.WriteLine("Enter new name:");
                    studentDto.Name = ConsoleReadHelper.GetNonEmptyInput();
                }
                if (userChoose == "2")
                {
                    Console.WriteLine("Enter new surname");
                    studentDto.Surname = ConsoleReadHelper.GetNonEmptyInput();
                }
                if (userChoose == "3")
                {
                    Console.WriteLine("On your own risk!");
                    studentDto.Sex = ConsoleReadHelper.GetSex();
                }
                if (userChoose == "4")
                {
                    studentDto.BirthDate = ConsoleReadHelper.GetBirthDateTime();
                }
                if (userChoose == "5")
                {
                    courseDto.CourseName = GetExistingCourseName();
                    _studentService.RemoveStudentFromCourse(studentDto, courseDto.CourseName);
                }

                _studentService.UpdateStudent(studentDto);

                Console.WriteLine(ConsoleWriteHelper.StudentCurrentData(studentDto) + "\n");

                Console.WriteLine("Press any key to change other student's data, or press <Q> for back to main menu");

            } while (Console.ReadLine() != "Q");
        }

        private void ChangeCourseInfo()
        {
            var courseDto = new CourseDto();
            courseDto.CourseName = GetExistingCourseName();
            courseDto = _courseService.GetCourseDataByName(courseDto.CourseName);

            Console.WriteLine(ConsoleWriteHelper.CourseCurrentData(courseDto) + "\n");

            do
            {
                Console.WriteLine("Choose action:\n<1> Change course name\n<2> Change teacher name" +
                                  "\n<3> Change min homework % required\n<4> Change min presence % required" +
                                  "\n<5> Sign out student from course");

                var userChoose = ConsoleReadHelper.GetChangeDecision();

                if (userChoose == "1")
                {
                    courseDto.CourseName = GetNewCourseName(courseDto);
                }
                if (userChoose == "2")
                {
                    Console.WriteLine("Enter new teacher name");
                    courseDto.TeacherName = ConsoleReadHelper.GetNonEmptyInput();
                }
                if (userChoose == "3")
                {
                    Console.WriteLine("Enter new min homework % required");
                    courseDto.HomeworkRequired = ConsoleReadHelper.GetReqInt();
                }
                if (userChoose == "4")
                {
                    Console.WriteLine("Enter new min presence % required");
                    courseDto.PresenceRequired = ConsoleReadHelper.GetReqInt();
                }
                if (userChoose == "5")
                {
                    bool studentAlreadyExists;
                    var pesel = ConsoleReadHelper.GetPesel();

                    do
                    {
                        studentAlreadyExists = _studentService.CheckIfStudentExistByPesel(pesel);
                        if (!studentAlreadyExists)
                        {
                            Console.WriteLine("There's no student in DataBase with this PESEL. Try Again...");
                        }
                    } while (!studentAlreadyExists);
                    var studentDto = _studentService.GetStudentDataByPesel(pesel);
                    _studentService.RemoveStudentFromCourse(studentDto, courseDto.CourseName);
                }

                _courseService.UpdateCourse(courseDto);

                Console.WriteLine(ConsoleWriteHelper.CourseCurrentData(courseDto) + "\n");

                Console.WriteLine("Press any key to change other course data, or press <Q> for back to main menu");

            } while (Console.ReadLine() != "Q");

        }

        string GetExistingCourseName()
        {
            Console.WriteLine("Enter course name: ");

            string courseName;
            bool courseAlreadyExists;
            do
            {
                courseName = Console.ReadLine();
                courseAlreadyExists = _courseService.CheckIfCourseExistByName(courseName);
                if (!courseAlreadyExists)
                {
                    Console.WriteLine("There's no course in DataBase with this name. Try Again...");
                }
            } while (!courseAlreadyExists);

            return courseName;
        }

        string GetNewCourseName(CourseDto course)
        {
            Console.WriteLine("Enter course name: ");
            bool courseAlreadyExists;
            do
            {
                course.CourseName = RegExpValidation.GetCourseNameRegEx();
                courseAlreadyExists = _courseService.CheckIfCourseExistByName(course.CourseName);
                if (courseAlreadyExists)
                {
                    Console.WriteLine("There's already a course in DataBase with this name. Try Again...");
                }
            } while (courseAlreadyExists);

            return course.CourseName;
        }

        private void OnReportGenerated(ReportDto report)
        {
            ReportHandler?.Invoke(this, report);
        }

        public void ExportReport(ReportDto report)
        {
            string fileName = RegExpValidation.GetFileNameToSaveRegEx();
            var isCorrect = _reportService.ExportReportToFile(report, fileName);

            ConsoleWriteHelper.FileSaveSuccessMessage(isCorrect);
        }

        public void ReportJsonExportQuestion(object sender, ReportDto report)
        {
            Console.WriteLine("Export this report to JSON file? <Y/N>");
            var userInput = Console.ReadLine();

            while (userInput != "Y" && userInput != "N")
            {
                Console.WriteLine("Try again...");
                userInput = Console.ReadLine();
            }

            if (userInput == "Y")
            {
                ExportReport(report);
            }
        }
    }
}
