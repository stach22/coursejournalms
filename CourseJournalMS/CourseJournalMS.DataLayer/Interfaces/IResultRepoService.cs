﻿using CourseJournalMS.DataLayer.Models;

namespace CourseJournalMS.DataLayer.Interfaces
{
    public interface IResultRepoService
    {
        int GetPointsSumForStudentInCourse(Student student, Course course);
    }
}