using CourseJournalMS.DataLayer.Models;

namespace CourseJournalMS.DataLayer.Interfaces
{
    public interface IPresencesRepoService
    {
        int GetPresenceForStudentInCourse(Student student, Course course);
    }
}