﻿namespace CourseJournalMS.DataLayer.Interfaces
{
    public interface IEntity
    {
        int Id { get; }
    }
}