﻿namespace CourseJournalMS.DataLayer.Interfaces
{
    public interface IContentMapper<T>
    {
        string FromContent(T content);
        T ToContent(string report);
    }
}