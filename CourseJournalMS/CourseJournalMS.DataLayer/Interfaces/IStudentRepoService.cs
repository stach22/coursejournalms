﻿using System.Linq;
using CourseJournalMS.DataLayer.Models;

namespace CourseJournalMS.DataLayer.Interfaces
{
    public interface IStudentRepoService
    {
        IQueryable<Student> GetStudentDataIncludingCourses();
        IQueryable<Student> GetStudentData();
    }
}