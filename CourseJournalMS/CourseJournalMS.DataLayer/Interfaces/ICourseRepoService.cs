using System.Linq;
using CourseJournalMS.DataLayer.Models;

namespace CourseJournalMS.DataLayer.Interfaces
{
    public interface ICourseRepoService
    {
        IQueryable<Course> GetAllCourseData();
        IQueryable<Course> GetCourseData();
        void RemoveStudentFromCourse(Course course, Student student);
    }
}