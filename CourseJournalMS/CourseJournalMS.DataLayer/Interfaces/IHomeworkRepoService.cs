﻿using CourseJournalMS.DataLayer.Models;

namespace CourseJournalMS.DataLayer.Interfaces
{
    public interface IHomeworkRepoService
    {
        int GetMaxPointsSumInCourse(Course course);
    }
}