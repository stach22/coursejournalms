using System.Data.Entity;
using System.Linq;

namespace CourseJournalMS.DataLayer.Interfaces
{
    public interface IGenericRepo<T> where T : class, IEntity
    {
        DbSet<T> DataSet { get; }
        void Add(T data);
        T GetById(int id);
        IQueryable<T> GetAll();
        void Update(T data);
        void Delete(int id);
    }
}