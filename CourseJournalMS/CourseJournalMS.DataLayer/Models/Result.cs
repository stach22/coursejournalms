﻿using CourseJournalMS.DataLayer.Interfaces;

namespace CourseJournalMS.DataLayer.Models
{
    public class Result : IEntity
    {
        public int Id { get; set; }
        public virtual Homework Homework { get; set; }
        public virtual Student Student { get; set; }
        public int Value { get; set; }
    }
}
