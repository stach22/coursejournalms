﻿using System;
using System.Collections.Generic;
using CourseJournalMS.DataLayer.Interfaces;

namespace CourseJournalMS.DataLayer.Models
{
    public class CourseDay : IEntity
    {
        public int Id { get; set; }
        public DateTime CourseDayDate { get; set; }

        public virtual Course Course { get; set; }
        public virtual List<Presence> Presences { get; set; } = new List<Presence>();
    }
}
