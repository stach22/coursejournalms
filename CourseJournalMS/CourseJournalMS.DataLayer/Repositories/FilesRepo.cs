﻿using System.IO;
using CourseJournalMS.DataLayer.Interfaces;

namespace CourseJournalMS.DataLayer.Repositories
{
    public class FilesRepo<T>
    {
        private readonly IContentMapper<T> _mapper;

        public FilesRepo(IContentMapper<T> contentMapper)
        {
            _mapper = contentMapper;
        }

        public void Save(string filePath, T content)
        {
            var stringToSave = _mapper.FromContent(content);
            File.WriteAllText(filePath, stringToSave);
        }

        public T Read(string filepath)
        {
            var read = File.ReadAllText(filepath);
            return _mapper.ToContent(read);
        }
    }
}
