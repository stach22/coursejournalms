﻿using System;
using System.Collections.Generic;
using CourseJournalMS.DataLayer.DbContexts;
using CourseJournalMS.DataLayer.Interfaces;
using CourseJournalMS.DataLayer.Models;

namespace CourseJournalMS.DataLayer.Repositories
{
    public class UnitOfWorkCourseJournal
    {
        private readonly CourseJournalMSDbContext _dbContext;
        private Dictionary<Type, object> _repositories;

        private static UnitOfWorkCourseJournal _instance;

        public static UnitOfWorkCourseJournal GetInstance()
        {
            if (_instance == null)
            {
                _instance = new UnitOfWorkCourseJournal();
            }
            return _instance;
        }

        private UnitOfWorkCourseJournal()
        {
            _dbContext = new CourseJournalMSDbContext();
            RegisterRepositories();
        }

        private void RegisterRepositories()
        {
            _repositories = new Dictionary<Type, object>();
            Register(typeof(Course), new GenericRepo<Course>(_dbContext));
            Register(typeof(CourseDay), new GenericRepo<CourseDay>(_dbContext));
            Register(typeof(Homework), new GenericRepo<Homework>(_dbContext));
            Register(typeof(Presence), new GenericRepo<Presence>(_dbContext));
            Register(typeof(Result), new GenericRepo<Result>(_dbContext));
            Register(typeof(Student), new GenericRepo<Student>(_dbContext));

        }

        private void Register(Type type, object repository)
        {
            if (_repositories.ContainsKey(type))
            {
                throw new Exception("There's already repo for type " + type.Name);
            }

            _repositories.Add(type, repository);
        }

        public GenericRepo<T> GetRepo<T>() where T : class, IEntity
        {
            return _repositories[typeof(T)] as GenericRepo<T>;
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }
    }
}
