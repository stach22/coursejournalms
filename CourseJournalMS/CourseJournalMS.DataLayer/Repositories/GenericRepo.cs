﻿using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CourseJournalMS.DataLayer.DbContexts;
using CourseJournalMS.DataLayer.Interfaces;

namespace CourseJournalMS.DataLayer.Repositories
{
    public class GenericRepo<T> : IGenericRepo<T> where T : class, IEntity
    {
        private readonly CourseJournalMSDbContext _dbContext;
        public DbSet<T> DataSet => _dbContext.Set<T>();

        public GenericRepo(CourseJournalMSDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Add(T data)
        {
            DataSet.Add(data);
        }

        public T GetById(int id)
        {
            return GetAll().SingleOrDefault(e => e.Id == id);
        }

        public IQueryable<T> GetAll()
        {
            return DataSet;
        }

        public void Update(T data)
        {
            DataSet.AddOrUpdate(data);
        }

        public void Delete(int id)
        {
            var toDelete = GetById(id);
            DataSet.Remove(toDelete);
        }
    }
}
