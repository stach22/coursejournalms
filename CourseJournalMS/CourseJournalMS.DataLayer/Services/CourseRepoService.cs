﻿using System.Data.Entity;
using System.Linq;
using CourseJournalMS.DataLayer.Interfaces;
using CourseJournalMS.DataLayer.Models;
using CourseJournalMS.DataLayer.Repositories;

namespace CourseJournalMS.DataLayer.Services
{
    public class CourseRepoService : ICourseRepoService
    {
        private readonly IGenericRepo<Course> _courseRepo;
        private readonly IGenericRepo<Student> _studentRepo;

        public CourseRepoService()
        {
            var unitOfWork = UnitOfWorkCourseJournal.GetInstance();

            var courseRepo = unitOfWork.GetRepo<Course>();
            _courseRepo = courseRepo;
            var studentRepo = unitOfWork.GetRepo<Student>();
            _studentRepo = studentRepo;
        }

        public IQueryable<Course> GetAllCourseData()
        {
            var course = _courseRepo.GetAll();

            return course.Include(c => c.CourseDays).Include(c => c.Homeworks).Include(c => c.Students);
        }

        public IQueryable<Course> GetCourseData()
        {
            var course = _courseRepo.GetAll();

            return course;
        }

        public void RemoveStudentFromCourse(Course course, Student student)
        {
            var studentDb = (from s in _studentRepo.DataSet where s.Id == student.Id select s).FirstOrDefault();
            var courseDb = (from c in _courseRepo.DataSet where c.Id == course.Id select c).FirstOrDefault();

            studentDb.Courses.Remove(courseDb);
            courseDb.Students.Remove(studentDb);
        }
    }
}
