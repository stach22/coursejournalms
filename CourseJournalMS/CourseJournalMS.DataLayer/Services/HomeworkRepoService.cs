﻿using System.Linq;
using CourseJournalMS.DataLayer.Interfaces;
using CourseJournalMS.DataLayer.Models;
using CourseJournalMS.DataLayer.Repositories;

namespace CourseJournalMS.DataLayer.Services
{
    public class HomeworkRepoService : IHomeworkRepoService
    {
        private readonly IGenericRepo<Homework> _homeworkRepo;

        public HomeworkRepoService()
        {
            var unitOfWork = UnitOfWorkCourseJournal.GetInstance();

            var homeworkRepo = unitOfWork.GetRepo<Homework>();
            _homeworkRepo = homeworkRepo;
        }

        public int GetMaxPointsSumInCourse(Course course)
        {
            var maxPointsSum = 0;

            var homeworkDbSet = _homeworkRepo.DataSet;

            if (homeworkDbSet.Any())
            {
                maxPointsSum = homeworkDbSet
                    .Where(h => h.Course.Id == course.Id)
                    .Sum(h => h.MaxPoints);
            }

            return maxPointsSum;
        }
    }
}
