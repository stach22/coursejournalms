﻿using System.Data.Entity;
using System.Linq;
using CourseJournalMS.DataLayer.Interfaces;
using CourseJournalMS.DataLayer.Models;
using CourseJournalMS.DataLayer.Repositories;

namespace CourseJournalMS.DataLayer.Services
{
    public class StudentRepoService : IStudentRepoService
    {
        private readonly IGenericRepo<Student> _studentRepo;

        public StudentRepoService()
        {
            var unitOfWork = UnitOfWorkCourseJournal.GetInstance();

            var studentRepo = unitOfWork.GetRepo<Student>();
            _studentRepo = studentRepo;
        }

        public IQueryable<Student> GetStudentDataIncludingCourses()
        {
            var student = _studentRepo.GetAll();

            return student.Include(s => s.Courses);
        }

        public IQueryable<Student> GetStudentData()
        {
            var student = _studentRepo.GetAll().Select(s => s);

            return student;
        }
    }
}
