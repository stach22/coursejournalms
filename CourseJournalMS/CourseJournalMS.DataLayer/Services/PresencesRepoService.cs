﻿using System.Linq;
using CourseJournalMS.DataLayer.Interfaces;
using CourseJournalMS.DataLayer.Models;
using CourseJournalMS.DataLayer.Repositories;

namespace CourseJournalMS.DataLayer.Services
{
    public class PresencesRepoService : IPresencesRepoService
    {
        private readonly IGenericRepo<Presence> _presenceRepo;

        public PresencesRepoService()
        {
            var unitOfWork = UnitOfWorkCourseJournal.GetInstance();

            var presencesRepo = unitOfWork.GetRepo<Presence>();
            _presenceRepo = presencesRepo;
        }

        public int GetPresenceForStudentInCourse(Student student, Course course)
        {
            var presences = 0;

            var presenceDbSet = _presenceRepo.DataSet;

            if (presenceDbSet.Any())
            {
                presences = presenceDbSet
                    .Where(p => p.Student.Id == student.Id)
                    .Where(p => p.CourseDay.Course.Id == course.Id)
                    .Count(p => p.Value == Presence.PresenceEnum.Present);
            }

            return presences;
        }
    }
}
