﻿using System.Linq;
using CourseJournalMS.DataLayer.Interfaces;
using CourseJournalMS.DataLayer.Models;
using CourseJournalMS.DataLayer.Repositories;

namespace CourseJournalMS.DataLayer.Services
{
    public class ResultRepoService : IResultRepoService
    {
        private readonly IGenericRepo<Result> _resultRepo;

        public ResultRepoService()
        {
            var unitOfWork = UnitOfWorkCourseJournal.GetInstance();

            var resultRepo = unitOfWork.GetRepo<Result>();
            _resultRepo = resultRepo;
        }

        public int GetPointsSumForStudentInCourse(Student student, Course course)
        {
            var pointsSum = 0;

            var resultDbSet = _resultRepo.DataSet;

            if (resultDbSet.Any())
            {
                pointsSum = resultDbSet
                    .Where(r => r.Student.Id == student.Id)
                    .Where(r => r.Homework.Course.Id == course.Id)
                    .Sum(r => r.Value);
            }

            return pointsSum;
        }
    }
}
