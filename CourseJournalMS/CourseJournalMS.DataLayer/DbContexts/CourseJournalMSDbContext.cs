﻿using System.Configuration;
using System.Data.Entity;
using CourseJournalMS.DataLayer.Models;

namespace CourseJournalMS.DataLayer.DbContexts
{
    public class CourseJournalMSDbContext : DbContext
    {
        public CourseJournalMSDbContext() : base(GetConnectionString())
        {
        }

        public DbSet<Course> CourseDbSet { get; set; }
        public DbSet<Student> StudentDbSet { get; set; }
        public DbSet<Homework> HomeworkDbSet { get; set; }
        public DbSet<Result> ResultDbSet { get; set; }
        public DbSet<CourseDay> CourseDayDbSet { get; set; }
        public DbSet<Presence> PresencesDbSet { get; set; }

        public static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MyDatabase"].ConnectionString;
        }
    }
}