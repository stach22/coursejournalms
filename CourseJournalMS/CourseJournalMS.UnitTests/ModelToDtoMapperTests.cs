﻿using System;
using CourseJournalMS.BusinessLayer.Dtos;
using CourseJournalMS.BusinessLayer.Mappers;
using CourseJournalMS.DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CourseJournalMS.UnitTests
{
    [TestClass]
    public class ModelToDtoMapperTests
    {
        [TestMethod]
        public void StudentMapping_ProvideValidStudent_ReceiveMappedStudentDto()
        {
            var studentToMap = new Student();
            var expectedResult = new StudentDto();

            studentToMap.Id = 8;
            studentToMap.Pesel = 74747474747;
            studentToMap.Name = "Marian";
            studentToMap.Surname = "Kuciapa";
            studentToMap.Sex = "M";
            studentToMap.BirthDate = DateTime.Parse("1982-11-08");

            expectedResult.Id = 8;
            expectedResult.Pesel = 74747474747;
            expectedResult.Name = "Marian";
            expectedResult.Surname = "Kuciapa";
            expectedResult.Sex = "M";
            expectedResult.BirthDate = DateTime.Parse("1982-11-08");

            var studentMapped = ModelToDtoMapper.ModelToStudentDto(studentToMap);

            Assert.AreEqual(expectedResult, studentMapped);
        }

        [TestMethod]
        public void CourseMapping_ProvideValidCourse_ReceiveMappedCourseDto()
        {
            var courseToMap = new Course();
            var expectedResult = new CourseDto();

            courseToMap.Id = 4;
            courseToMap.CourseName = "C# Junior Dev";
            courseToMap.TeacherName = "Huang Chong Un";
            courseToMap.StartTime = DateTime.Parse("2017-04-24");
            courseToMap.HomeworkRequired = 85;
            courseToMap.PresenceRequired = 90;

            expectedResult.Id = 4;
            expectedResult.CourseName = "C# Junior Dev";
            expectedResult.TeacherName = "Huang Chong Un";
            expectedResult.StartTime = DateTime.Parse("2017-04-24");
            expectedResult.HomeworkRequired = 85;
            expectedResult.PresenceRequired = 90;

            var courseMapped = ModelToDtoMapper.CourseModelToDtoMapper(courseToMap);

            Assert.AreEqual(expectedResult, courseMapped);
        }
    }
}
