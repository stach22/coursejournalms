﻿using System;
using System.Collections.Generic;
using CourseJournalMS.BusinessLayer.Services;
using CourseJournalMS.DataLayer.Interfaces;
using CourseJournalMS.DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CourseJournalMS.UnitTests
{
    [TestClass]
    public class ReportServiceTests
    {
        [TestMethod]
        public void CourseReporting_ProvideAllCourseInfo_GetValidReport()
        {
            var course = new Course();
            var course2 = new Course();
            var student1 = new Student();
            var student2 = new Student();
            var student3 = new Student();
            var homework1 = new Homework();
            var homework2 = new Homework();
            var result1 = new Result();
            var result2 = new Result();
            var result3 = new Result();
            var result4 = new Result();
            var courseDay1 = new CourseDay();
            var courseDay2 = new CourseDay();
            var presence1 = new Presence();
            var presence2 = new Presence();
            var presence3 = new Presence();
            var presence4 = new Presence();

            course.Id = 3;
            course.CourseName = "C# Junior Dev";
            course.TeacherName = "Konstanty Idefons";
            course.StartTime = DateTime.Parse("2017-04-14");
            course.HomeworkRequired = 80;
            course.PresenceRequired = 85;

            course2.Id = 5;
            course2.CourseName = "Java";
            course2.TeacherName = "Huang Chung";
            course2.StartTime = DateTime.Parse("2017-05-20");
            course2.HomeworkRequired = 70;
            course2.PresenceRequired = 70;

            student1.Id = 8;
            student1.Pesel = 74747474747;
            student1.Name = "Marian";
            student1.Surname = "Kuciapa";
            student1.Sex = "M";
            student1.BirthDate = DateTime.Parse("1982-11-08");

            student2.Id = 2;
            student2.Pesel = 83838383838;
            student2.Name = "Stefan";
            student2.Surname = "Malinowski";
            student2.Sex = "M";
            student2.BirthDate = DateTime.Parse("1992-07-02");

            student3.Id = 3;
            student3.Pesel = 92929292929;
            student3.Name = "Kazimierz";
            student3.Surname = "Masztalerz";
            student3.Sex = "F";
            student3.BirthDate = DateTime.Parse("1972-05-28");

            course.Students = new List<Student>();
            course.Students.Add(student1);
            course.Students.Add(student2);
            course.Students.Add(student3);

            course2.Students = new List<Student>();
            course2.Students.Add(student2);

            homework1.Id = 1;
            homework1.HomeworkName = "H1";
            homework1.MaxPoints = 50;

            result1.Id = 1;
            result1.Student = student1;
            result1.Homework = homework1;
            result1.Value = 48;
            result2.Id = 2;
            result2.Student = student2;
            result2.Homework = homework1;
            result2.Value = 36;

            homework2.Id = 2;
            homework2.HomeworkName = "H2";
            homework2.MaxPoints = 70;

            result3.Id = 3;
            result3.Student = student1;
            result3.Homework = homework2;
            result3.Value = 68;
            result4.Id = 4;
            result4.Student = student2;
            result4.Homework = homework2;
            result4.Value = 53;

            homework1.Results.Add(result1);
            homework1.Results.Add(result2);
            homework2.Results.Add(result3);
            homework2.Results.Add(result4);

            courseDay1.Id = 1;
            courseDay1.CourseDayDate = DateTime.Parse("2017-04-15");

            courseDay2.Id = 2;
            courseDay2.CourseDayDate = DateTime.Parse("2017-04-16");

            course.CourseDays.Add(courseDay1);
            course.CourseDays.Add(courseDay2);

            presence1.Id = 1;
            presence1.CourseDay = courseDay1;
            presence1.Student = student1;
            presence1.Value = Presence.PresenceEnum.Present;
            presence2.Id = 2;
            presence2.CourseDay = courseDay1;
            presence2.Student = student2;
            presence2.Value = Presence.PresenceEnum.Present;

            presence3.Id = 3;
            presence3.CourseDay = courseDay2;
            presence3.Student = student1;
            presence3.Value = Presence.PresenceEnum.Present;
            presence4.Id = 4;
            presence4.CourseDay = courseDay2;
            presence4.Student = student2;
            presence4.Value = Presence.PresenceEnum.Absent;

            courseDay1.Presences.Add(presence1);
            courseDay1.Presences.Add(presence2);
            courseDay2.Presences.Add(presence3);
            courseDay2.Presences.Add(presence4);

            var homeworkMessage = "8 Marian Kuciapa 116/120 (96,67%) - Passed" +
                                  "\n2 Stefan Malinowski 89/120 (74,17%) - Failed" +
                                  "\n3 Kazimierz Masztalerz 0/120 (0,00%) - Failed\n";

            var presencesMessage = "8 Marian Kuciapa 2/2 (100,00%) - Passed" +
                                   "\n2 Stefan Malinowski 1/2 (50,00%) - Failed" +
                                   "\n3 Kazimierz Masztalerz 0/2 (0,00%) - Failed\n";

            var getReportMessage =
                "Course name: C# Junior Dev\nCourse Teacher: Konstanty Idefons\nCourse start date: 2017-04-14 00:00:00" +
                "\nCourse presence required: 85%\nCourse homework required: 80%\n\nEach student\'s presence: " +
                "\n8 Marian Kuciapa 2/2 (100,00%) - Passed\n2 Stefan Malinowski 1/2 (50,00%) - Failed" +
                "\n3 Kazimierz Masztalerz 0/2 (0,00%) - Failed\n\nEach student\'s homeworks done: " +
                "\n8 Marian Kuciapa 116/120 (96,67%) - Passed\n2 Stefan Malinowski 89/120 (74,17%) - Failed" +
                "\n3 Kazimierz Masztalerz 0/120 (0,00%) - Failed\n";

            var getReportMessage2 =
                "Course name: Java\nCourse Teacher: Huang Chung\nCourse start date: 2017-05-20 00:00:00" +
                "\nCourse presence required: 70%\nCourse homework required: 70%\n\nEach student\'s presence: " +
                "\n2 Stefan Malinowski 0/0 (0,00%) - Failed\n\nEach student\'s homeworks done: " +
                "\n2 Stefan Malinowski 0/0 (0,00%) - Failed\n";

            //var homeworkRepoMock = new Mock<IHomeworkRepo>();
            //homeworkRepoMock.Setup((x) => x.GetMaxPointsSumInCourse(course)).Returns(120);

            //var resultRepoMock = new Mock<IResultRepo>();
            //resultRepoMock.Setup(x => x.GetPointsSumForStudentInCourse(student1, course)).Returns(116);
            //resultRepoMock.Setup(x => x.GetPointsSumForStudentInCourse(student2, course)).Returns(89);

            //var presencesRepoMock = new Mock<IPresencesRepo>();
            //presencesRepoMock.Setup(x => x.GetPresenceForStudentInCourse(student1, course)).Returns(2);
            //presencesRepoMock.Setup(x => x.GetPresenceForStudentInCourse(student2, course)).Returns(1);

            //var courseRepoMock = new Mock<ICourseRepo>();
            //courseRepoMock.Setup(c => c.GetCourseByName(course.CourseName)).Returns(course);

            //var courseRepoMock2 = new Mock<ICourseRepo>();
            //courseRepoMock2.Setup(c => c.GetCourseByName(course2.CourseName)).Returns(course2);

            //var homeworkRepoMock2 = new Mock<IHomeworkRepo>();
            //homeworkRepoMock2.Setup(x => x.GetMaxPointsSumInCourse(course2)).Returns(0);

            //var resultRepoMock2 = new Mock<IResultRepo>();
            //resultRepoMock2.Setup(x => x.GetPointsSumForStudentInCourse(student2, course2)).Returns(0);

            //var presencesRepoMock2 = new Mock<IPresencesRepo>();
            //presencesRepoMock2.Setup(x => x.GetPresenceForStudentInCourse(student2, course2)).Returns(0);

            //var expectedResult = new ReportService(homeworkRepoMock.Object, resultRepoMock.Object, presencesRepoMock.Object, courseRepoMock.Object);

            //var expectedResult2 = new ReportService(homeworkRepoMock2.Object, resultRepoMock2.Object, presencesRepoMock2.Object, courseRepoMock2.Object);

            //Assert.AreEqual(homeworkMessage, expectedResult.StudentsHomeworks(course));
            //Assert.AreEqual(presencesMessage, expectedResult.StudentsPresence(course));
            //Assert.AreEqual(getReportMessage, expectedResult.GetReport(course.CourseName));
            //Assert.AreEqual(getReportMessage2, expectedResult2.GetReport(course2.CourseName));
        }
    }
}
